﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6.Humanity
{
    internal class Human : IHasName
    {   
        public string Name { get; set; }
        public string Surname { get; set; }

        public string GetName()
        {
            return "Name";
        }
    }
}
