﻿using lab6.Humanity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    class CoupleAttribute : Attribute
    {
        public string Pair { get; set; }
        public double Probability { get; set; }
        public string ChildType { get; set; }

        public CoupleAttribute(string Pair, double Probability, string ChildType)
        {
            this.Pair = Pair;
            this.Probability = Probability;
            this.ChildType = ChildType;
        }

        public static IHasName Couple(Human left, Human right)
        {
            CoupleAttribute leftAttribute = null;
            CoupleAttribute[] leftAttributes = (CoupleAttribute[])GetCustomAttributes(left.GetType(), typeof(CoupleAttribute));
            CoupleAttribute[] rightAttributes = (CoupleAttribute[])GetCustomAttributes(right.GetType(), typeof(CoupleAttribute));

            var isLeftLove = false;
            var isRightLove = false;

            foreach (var attribute in leftAttributes)
            {
                if (attribute.Pair.Equals(right.GetType().Name))
                {
                    leftAttribute = attribute;
                    var rand = UniqueRandom.Instance.Next(100);
                    isLeftLove = rand < attribute.Probability * 100;
                }
            }

            foreach (var attribute in rightAttributes)
            {
                if (attribute.Pair.Equals(left.GetType().Name))
                {
                    var rand = UniqueRandom.Instance.Next(100);
                    isRightLove = rand < attribute.Probability * 100;
                }
            }

            MethodInfo[] methods = right.GetType().GetMethods();
            MethodInfo targetMethod = methods.Where(x => x.ReturnType.Equals(typeof(string))).ToList()[0];
            
            var name = targetMethod.Invoke(right, new object[] { });
            var child = Assembly.GetExecutingAssembly().CreateInstance("lab6.Humanity." + leftAttribute.ChildType);
            (child as Human).Name = name as string;

            var type = child.GetType();
            if (type.Equals(typeof(Botan)) || type.Equals(typeof(Student)))
            {
                (child as Human).Surname = "ович";
            }
            else if (type.Equals(typeof(Girl)) || type.Equals(typeof(PrettyGirl)) || type.Equals(typeof(SmartGirl)))
            {
                (child as Human).Surname = "івна";
            }
            else
            {
                // Book
            }

            Console.WriteLine(child.GetType());

            return child as IHasName;
        }
    }
}
