﻿using lab6.Humanity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            CoupleAttribute.Couple(new Student(), new PrettyGirl());
            //CoupleAttribute.Couple(new PrettyGirl(), new Botan());

            Console.ReadLine();
        }
    }
}
