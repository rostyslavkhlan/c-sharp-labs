﻿using System;
using lab1.models;

namespace lab1 {
    internal static class Program {
        public static void Main() {
            Student student = new Student(new Person("Rostyslav", "Khlan", new DateTime(1998, 6, 17)), Education.Bachelor, 407);
            Console.WriteLine(student.ToShortString());

            Console.WriteLine("Master = " + student[Education.Master]);
            Console.WriteLine("Bachelor = " + student[Education.Bachelor]);
            Console.WriteLine("Second education = " + student[Education.SecondEducation]);

            student.AddExams(new Exam(), new Exam(), new Exam());
            Console.WriteLine(student);

            Console.Write("Enter rows and columns: ");
            var parameters = Console.ReadLine().Split(' ');
            int rows = Convert.ToInt32(parameters[0]);
            int columns = Convert.ToInt32(parameters[1]);


            Exam[] oneDimensionalArray = new Exam[rows * columns];
            var start = Environment.TickCount;
            for (var i = 0; i < oneDimensionalArray.Length; ++i)
                oneDimensionalArray[i] = new Exam();
            var end = Environment.TickCount;
            Console.WriteLine("One dimensional: " + (end - start));


            Exam[,] twoDimensionalArray = new Exam[rows, columns];
            start = Environment.TickCount;
            for (var i = 0; i < rows; ++i)
                for (var j = 0; j < columns; ++j)
                    twoDimensionalArray[i, j] = new Exam();
            end = Environment.TickCount;
            Console.WriteLine("Two dimensional: " + (end - start));


            int sum = 0;
            int size = 0;
            while (sum < rows * columns) { sum += ++size; }
            Exam[][] gearArray = new Exam[size][];
            for (int i = 0; i < size; i++) {
                if (i == size - 1) {
                    gearArray[i] = new Exam[rows * columns - (sum - size)];
                    break;
                }
                gearArray[i] = new Exam[i + 1];
            }

            start = Environment.TickCount;
            foreach (var lineArray in gearArray) {
                for (var j = 0; j < lineArray.Length; j++) {
                    lineArray[j] = new Exam();
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            end = Environment.TickCount;
            Console.WriteLine("Gear array: " + (end - start));
        }
    }
}