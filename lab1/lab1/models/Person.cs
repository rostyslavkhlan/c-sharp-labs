using System;

namespace lab1.models {
    public class Person {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthdayDate { get; set; }

        public Person() : this("First name", "Last name", new DateTime()){}
        
        public Person(string firstName, string lastName, DateTime birthdayDate) {
            FirstName = firstName;
            LastName = lastName;
            BirthdayDate = birthdayDate;
        }
        
        public int BirthdayDateYear {
            get { return BirthdayDate.Year; }
            set { BirthdayDate = new DateTime(value, BirthdayDate.Month, BirthdayDate.Day); }
        }
 
        public override string ToString() {
            return FirstName + " " + LastName + ". " + BirthdayDate;
        }
        
        public virtual string ToShortString() {
            return FirstName + " " + LastName;
        }
    }
}