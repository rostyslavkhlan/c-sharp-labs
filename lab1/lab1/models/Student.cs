using System;
using System.Linq;
using System.Text;

namespace lab1.models {
    public class Student  {
        public Person Person { get; set; }
        public Education EducationForm { get; set; }
        public int GroupNumber { get; set; }
        private Exam[] _exams;

        public Exam[] Exams {
            get { return _exams; }
            set { _exams = value;  }
        }

        public Student() : this(new Person(), Education.Bachelor, 100){}

        public Student(Person person, Education educationForm, int groupNumber) {
            Person = person;
            EducationForm = educationForm;
            GroupNumber = groupNumber;
            Exams = new Exam[]{};
        }

        public bool this[Education education] {
            get { return education == EducationForm; }
        }
        
        public double AverageMark {
            get {
                if (Exams == null || Exams.Length == 0)
                    return 0;
                return Exams.Sum(exam => exam.Mark) / Exams.Length;
            }
        }
        
        public void AddExams(params Exam[] exams) {
            if (exams == null || exams.Length == 0) return;
            Array.Resize(ref _exams, Exams.Length + exams.Length);
            Array.Copy(exams, 0, Exams, Exams.Length - exams.Length, exams.Length);
        }

        public override string ToString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Person.ToString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            foreach (Exam exam in Exams)
                stringBuilder.AppendLine(exam.ToString());
            return stringBuilder.ToString();
        }

        public virtual string ToShortString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Person.ToString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Average mark: " + AverageMark);
            return stringBuilder.ToString();
        }
    }
}