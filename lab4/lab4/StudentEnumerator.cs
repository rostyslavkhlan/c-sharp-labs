using System.Collections;
using System.Collections.Generic;
using System.Linq;
using lab4.models;

namespace lab4 {
    public class StudentEnumerator : IEnumerator {
        private List<string> Subjects { get; set; }
        private int _index = -1;
        
        public StudentEnumerator(List<Test> tests, List<Exam> exams) {
            Subjects = new List<string>();
            var testsName = tests.Select(f => f.SubjectName).Distinct().ToList();
            var examsName = exams.Select(f => f.SubjectName).Distinct().ToList();
            
            Subjects.AddRange(testsName);
            foreach (var name in testsName) {
                if (!examsName.Contains(name))
                    Subjects.Remove(name);
            }
        }
        
        public bool MoveNext() {
            _index++;    
            return _index < Subjects.Count;
        }

        public void Reset() {
            _index = -1;
        }

        public object Current {
            get { return Subjects[_index]; }
        }
    }
}