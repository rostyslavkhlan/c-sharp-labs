using System.Collections.Generic;
using System.Text;
using lab4.models;

namespace lab4 {
    public class Journal {
        private readonly List<JournalEntry> _entries = new List<JournalEntry>();

        public void OnChange(object sender, StudentListHandlerEventArgs eventArgs) {
            _entries.Add(new JournalEntry(eventArgs.CollectionName, eventArgs.CollectionChangeType, eventArgs.Student));
        }

        public override string ToString() {
            var stringBuilder = new StringBuilder();
            foreach (var entry in _entries) {
                stringBuilder.AppendLine(entry.ToString());
            }
            return stringBuilder.ToString();
        }
    }
}