﻿using System;
using System.Collections.Generic;
using lab4.models;

namespace lab4 {
    internal static class Program {
        public static void Main() {
            var firstCollection = new StudentCollection("First collection", new List<Student>());
            var secondCollection = new StudentCollection("Second collection", new List<Student>());
            
            var firstJournal = new Journal();
            var secondJournal = new Journal();

            firstCollection.StudentCountChanged += firstJournal.OnChange;
            firstCollection.StudentReferenceChanged += firstJournal.OnChange;
            
            secondCollection.StudentCountChanged += secondJournal.OnChange;
            secondCollection.StudentReferenceChanged += secondJournal.OnChange;
            
            firstCollection.AddDefaults();
            firstCollection.Remove(2);
            firstCollection.Remove(11);
            firstCollection[2] = new Student();
            
            secondCollection.AddDefaults();
            secondCollection.Remove(2);
            secondCollection.AddDefaults();
            secondCollection.Remove(5);
            secondCollection[4] = new Student();
            
            Console.WriteLine(firstJournal);
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine(secondJournal);
            
            var s1 = new Student();
            var s2 = (Student)s1.DeepCopy();
            Console.WriteLine(s1.Equals(s2));
        }
    }
}