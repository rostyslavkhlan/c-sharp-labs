using System.Collections.Generic;
using lab4.models;

namespace lab4 {
    public class StudentComparer : IComparer<Student> {
        public int Compare(Student p1, Student p2) {
            if (p1.AverageMark() > p2.AverageMark())
                return 1;
            if (p1.AverageMark() < p2.AverageMark())
                return -1;
            return 0;
        }
    }
}