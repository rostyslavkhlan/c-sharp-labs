using System;
using lab4.models;

namespace lab4 {
    public class StudentListHandlerEventArgs : EventArgs {
        public string CollectionName { get; }
        public string CollectionChangeType { get; }
        public Student Student { get; }

        public StudentListHandlerEventArgs(string collectionName, string collectionChangeType, Student student) {
            CollectionName = collectionName;
            CollectionChangeType = collectionChangeType;
            Student = student;
        }

        public override string ToString() => $"CollectionName: {CollectionName}, CollectionChangeType: {CollectionChangeType}, Student: {Student.ToShortString()}";
    }
}