using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using lab4.models;

namespace lab4 {
    internal class StudentCollection {
        private const int DefaultsSize = 4;
        
        public string CollectionName { get; set; }
        public List<Student> ListStudent { get; }
        
        public delegate void StudentListHandler (object source, StudentListHandlerEventArgs args);
        public event StudentListHandler StudentCountChanged;
        public event StudentListHandler StudentReferenceChanged;
        
        public StudentCollection() : this("Name", new List<Student>()) {}

        public StudentCollection(string collectionName, List<Student> listStudent) {
            CollectionName = collectionName;
            ListStudent = listStudent;
        }

        public Student this[int index] {
            get => ListStudent[index];
            set {
                ListStudent[index] = value;
                OnStudentReferenceChanged(this, new StudentListHandlerEventArgs(CollectionName, "Item by index " + index + " was changed", value));
            }
        }

        public double MaxAverageMark => ListStudent.Max(mark => mark.AverageMark());

        public IEnumerable<Student> Masters => ListStudent.Where(education => education.EducationForm == Education.Master);

        public List<Student> GetStudentsByAverageMark(double value) => ListStudent.Where(mark => Math.Abs(mark.AverageMark() - value) < 0.001).ToList();

        public void AddDefaults() {
            for (var i = 0; i < DefaultsSize; ++i) {
                var student = new Student();
                ListStudent.Add(student);
                OnStudentCountChanged(this, new StudentListHandlerEventArgs(CollectionName, "New item added to collection. Collection size: " + ListStudent.Count, student));
            }
        }

        public void AddStudents(params Student[] students) {
            if (students == null) return;
            foreach (var student in students) {
                ListStudent.Add(student);
                OnStudentCountChanged(this, new StudentListHandlerEventArgs(CollectionName, "New item added to collection", student));
            }
        }

        public bool Remove(int index) {
            if (index >= ListStudent.Count) return false;
            var student = ListStudent[index];
            ListStudent.Remove(student);
            OnStudentCountChanged(this, new StudentListHandlerEventArgs(CollectionName, "Item removed from collection. Collection size: " + ListStudent.Count, student));
            return true;
        }
        
        public void SortByLastName() => ListStudent.Sort();

        public void SortByBirthDate() => ListStudent.Sort(new Person());

        public void SortByAverageMark() => ListStudent.Sort(new StudentComparer());

        public override string ToString() {
            var stringBuilder = new StringBuilder();
            foreach (var student in ListStudent)
                stringBuilder.AppendLine(student.ToString());
            return stringBuilder + "";
        }

        public string ToShortString() {
            var stringBuilder = new StringBuilder();
            foreach (var student in ListStudent) {
                stringBuilder.AppendLine(student.ToShortString());
                stringBuilder.AppendLine("Total tests: " + student.Tests.Count + "");
                stringBuilder.AppendLine("Total exams: " + student.Exams.Count + "");
            }
            return stringBuilder + "";
        }

        private void OnStudentCountChanged(object source, StudentListHandlerEventArgs args) {
            StudentCountChanged?.Invoke(source, args);
        }

        private void OnStudentReferenceChanged(object source, StudentListHandlerEventArgs args) {
            StudentReferenceChanged?.Invoke(source, args);
        }
    }
}