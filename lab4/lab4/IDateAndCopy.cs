using System;

namespace lab4 {
    public interface IDateAndCopy {
        object DeepCopy();
        DateTime Date { get; set; }
    }
    
    
}