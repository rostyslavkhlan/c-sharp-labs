namespace lab4.models {
    public class JournalEntry {
        private string CollectionName { get; }
        private string ChangeDescription { get; }
        private Student Student { get; }

        public JournalEntry(string collectionName, string changeDescription, Student student) {
            CollectionName = collectionName;
            ChangeDescription = changeDescription;
            Student = student;
        }

        public override string ToString() {
            return $"CollectionName: {CollectionName}, ChangeDescription: {ChangeDescription}, Student: {Student.FirstName + " " + Student.LastName}";
        }
    }
}