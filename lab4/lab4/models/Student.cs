using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace lab4.models {
    public class Student : Person, IEnumerable {
        public Education EducationForm { get; set; }
        private int _groupNumber;
        public List<Exam> Exams { get; set; }
        public List<Test> Tests { get; set; }

        public Student() : this(new Person(), Education.Bachelor, 100) {}

        public Student(Person person, Education educationForm, int groupNumber) : base(person.FirstName, person.LastName, person.BirthdayDate){
            EducationForm = educationForm;
            GroupNumber = groupNumber;
            Exams = new List<Exam>();
            Tests = new List<Test>();
        }

        public IEnumerator GetEnumerator() {
            return new StudentEnumerator(Tests, Exams);
        }

        public IEnumerable GetEnumerator(int required) {
            foreach (var exam in Exams) {
                if (exam.Mark > required) {
                    yield return exam;  
                }
            }
        }
        
        public IEnumerable GetPassedExamsAndTests() {
            foreach (var exam in GetEnumerator(2)) {
                yield return exam;
            }
            foreach (var test in Tests) {
                if (test.Passed) {
                    yield return test;  
                }
            }
        }
        
        public IEnumerable GetPassedTestsByPassedExams() {
            foreach (string name in this) {
                foreach (var test in Tests) {
                    if (test.SubjectName.Equals(name) && test.Passed) {
                        foreach (var exam in Exams) {
                            if (exam.SubjectName.Equals(name) && exam.Mark > 2)
                                yield return test;
                        }
                    }
                }
            }
        }
        
        public int GroupNumber {
            get => _groupNumber;
            set {
                if(value < 100 || value > 699)
                    throw new Exception("Group number must be more than 100 and less than 699 ");
                _groupNumber = value;
            }
        }

        public bool this[Education education] => education == EducationForm;

        public double AverageMark() {
            if (Exams.Count == 0) return 0;
            var averageMark = 0;
            foreach (var exam in Exams)
                averageMark += exam.Mark;
            return averageMark / (double)Exams.Count;
        }
        
        public void AddExams(params Exam[] exams) {
            if (exams != null) Exams.AddRange(exams);
        }
        
        public void AddTests(params Test[] tests) {
            if (tests != null) Tests.AddRange(tests);
        }
        
        public override string ToString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Exams:");
            foreach (var exam in Exams)
                stringBuilder.AppendLine(exam.ToString());
            stringBuilder.AppendLine("Tests:");
            foreach (var test in Tests)
                stringBuilder.AppendLine(test.ToString());
            return stringBuilder.ToString();
        }

        public override string ToShortString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToShortString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Average mark: " + AverageMark());
            return stringBuilder.ToString();
        }

        public override object DeepCopy() {
            var other = (Student)MemberwiseClone();
            other.Exams = new List<Exam>();
            foreach (var exam in Exams)
                other.Exams.Add((Exam)exam.DeepCopy());
            other.Tests = new List<Test>();
            foreach (var test in Tests)
                other.Tests.Add((Test)test.DeepCopy());
            return other;    
        }

        private bool Equals(Student other) => base.Equals(other) && _groupNumber == other._groupNumber &&
                                              EducationForm == other.EducationForm && Equals(Exams, other.Exams) &&
                                              Equals(Tests, other.Tests);

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Student)) return false;
            return Equals((Student) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ _groupNumber;
                hashCode = (hashCode * 397) ^ (int) EducationForm;
                hashCode = (hashCode * 397) ^ (Exams != null ? Exams.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tests != null ? Tests.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Student left, Student right) => ReferenceEquals(left, right);

        public static bool operator !=(Student left, Student right) => !ReferenceEquals(left, right);
    }
}