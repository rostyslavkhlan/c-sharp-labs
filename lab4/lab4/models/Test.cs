using System;

namespace lab4.models {
    public class Test : IDateAndCopy {
        public string SubjectName { get; set; }
        public bool Passed { get; set; }
        public DateTime Date { get; set; }

        public Test() : this("Default name", false) { }

        public Test(string subjectName, bool passed) {
            SubjectName = subjectName;
            Passed = passed;
        }

        public object DeepCopy() => MemberwiseClone();

        private bool Equals(Test other) => string.Equals(SubjectName, other.SubjectName) && Passed == other.Passed && Date.Equals(other.Date);

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Test) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (SubjectName != null ? SubjectName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Passed.GetHashCode();
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Test left, Test right) => ReferenceEquals(left, right);

        public static bool operator !=(Test left, Test right) => !ReferenceEquals(left, right);

        public override string ToString() => $"SubjectName: {SubjectName}, Passed: {Passed}";
    }
}