using System;

namespace lab5 {
    public interface IDateAndCopy<T> {
        T DeepCopy();
        DateTime Date { get; set; }
    }
    
    
}