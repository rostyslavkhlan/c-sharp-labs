using System;
using System.Collections.Generic;

namespace lab5.models {
    [Serializable]
    public class Person : IDateAndCopy<Person>, IComparable, IComparer<Person> {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthdayDate { get; set; }
        public virtual DateTime Date { get; set; }

        public Person() : this("First name", "Last name", DateTime.Today) { }

        public Person(string firstName, string lastName, DateTime birthdayDate) {
            FirstName = firstName;
            LastName = lastName;
            BirthdayDate = birthdayDate;
        }

        public int BirthdayDateYear {
            get => BirthdayDate.Year;
            set => BirthdayDate = new DateTime(value, BirthdayDate.Month, BirthdayDate.Day);
        }

        protected bool Equals(Person other) {
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && BirthdayDate.Equals(other.BirthdayDate) && Date.Equals(other.Date);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ BirthdayDate.GetHashCode();
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Person left, Person right) {
            return Equals(left, right);
        }

        public static bool operator !=(Person left, Person right) {
            return !Equals(left, right);
        }

        public Person DeepCopy() => (Person)MemberwiseClone();

        public int CompareTo(object ob) {
            if (ob != null)
                return string.Compare(LastName, ((Person)ob).LastName, StringComparison.Ordinal);
            throw new Exception("It's impossible to compare two objects");
        }

        public int Compare(Person p1, Person p2) {
            return DateTime.Compare(p1.BirthdayDate, p2.BirthdayDate);
        }
        
        public override string ToString() {
            return FirstName + " " + LastName + ". " + BirthdayDate;
        }

        public virtual string ToShortString() {
            return FirstName + " " + LastName;
        }
    }
}