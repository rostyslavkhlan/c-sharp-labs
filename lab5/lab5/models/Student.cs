using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace lab5.models {
    
    [Serializable]
    public class Student : Person, IEnumerable {

        #region Fields

        private int _groupNumber;

        #endregion Fields

        #region Properties

        public Education EducationForm { get; set; }

        public List<Exam> Exams { get; set; }

        public List<Test> Tests { get; set; }
    
        public int GroupNumber {
            get => _groupNumber;
            set {
                if (value < 100 || value > 699)
                    throw new Exception("Group number must be more than 100 and less than 699 ");
                _groupNumber = value;
            }
        }

        #endregion Properties 

        #region Constructors

        public Student() : this(new Person(), Education.Bachelor, 100) {}

        public Student(Person person, Education educationForm, int groupNumber) : base(person.FirstName, person.LastName, person.BirthdayDate){
            EducationForm = educationForm;
            GroupNumber = groupNumber;
            Exams = new List<Exam>();
            Tests = new List<Test>();
        }

        #endregion Constructors

        public bool this[Education education] => education == EducationForm;

        #region Methods

        public IEnumerator GetEnumerator() {
            return new StudentEnumerator(Tests, Exams);
        }

        public IEnumerable GetEnumerator(int required) {
            foreach (var exam in Exams) {
                if (exam.Mark > required) {
                    yield return exam;  
                }
            }
        }
        
        public IEnumerable GetPassedExamsAndTests() {
            foreach (var exam in GetEnumerator(2)) {
                yield return exam;
            }
            foreach (var test in Tests) {
                if (test.Passed) {
                    yield return test;  
                }
            }
        }
        
        public IEnumerable GetPassedTestsByPassedExams() {
            foreach (string name in this) {
                foreach (var test in Tests) {
                    if (test.SubjectName.Equals(name) && test.Passed) {
                        foreach (var exam in Exams) {
                            if (exam.SubjectName.Equals(name) && exam.Mark > 2)
                                yield return test;
                        }
                    }
                }
            }
        }

        public double AverageMark() {
            if (Exams.Count == 0) return 0;
            var averageMark = 0;
            foreach (var exam in Exams)
                averageMark += exam.Mark;
            return averageMark / (double)Exams.Count;
        }
        
        public void AddExams(params Exam[] exams) {
            if (exams != null) Exams.AddRange(exams);
        }
        
        public void AddTests(params Test[] tests) {
            if (tests != null) Tests.AddRange(tests);
        }
        
        public override string ToString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Exams:");
            foreach (var exam in Exams)
                stringBuilder.AppendLine(exam.ToString());
            stringBuilder.AppendLine("Tests:");
            foreach (var test in Tests)
                stringBuilder.AppendLine(test.ToString());
            return stringBuilder.ToString();
        }

        public override string ToShortString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToShortString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Average mark: " + AverageMark());
            return stringBuilder.ToString();
        }

        public new Student DeepCopy() {
            using (var stream = new MemoryStream()) {
                if (GetType().IsSerializable) {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                    stream.Position = 0;
                    return (Student)formatter.Deserialize(stream);
                }
                return null;
            }
        }

        public bool Save(string fileName) => Save(fileName, this);

        public bool Load(string fileName) => Load(fileName, this);

        public bool AddFromConsole() {
            Console.WriteLine("Hi. Enter the exam data for this template:");
            Console.WriteLine("Exam name,4,02/12/2018");

            var data = Console.ReadLine().Split(',');
            try {
                if (data.Length != 3) throw new Exception("Wrong message");

                AddExams(new Exam(data[0], int.Parse(data[1]), DateTime.Parse(data[2])));  
            } catch (Exception e) {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        private bool Equals(Student other) {
            return base.Equals(other) && _groupNumber == other._groupNumber &&
                   EducationForm == other.EducationForm && Exams.SequenceEqual(other.Exams) &&
                   Tests.SequenceEqual(other.Tests);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Student)) return false;
            return Equals((Student) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ _groupNumber;
                hashCode = (hashCode * 397) ^ (int) EducationForm;
                hashCode = (hashCode * 397) ^ (Exams != null ? Exams.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tests != null ? Tests.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Student left, Student right) => ReferenceEquals(left, right);

        public static bool operator !=(Student left, Student right) => !ReferenceEquals(left, right);

        public static bool Save(string fileName, Student student) {
            try {
                using (Stream stream = File.Open(fileName, FileMode.Create)) {
                    var binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(stream, student);
                }
            } catch (UnauthorizedAccessException) {
                Console.WriteLine("Error: Unauthorized access. Serialization has not been completed");
                return false;
            }
            return true;
        }
        
        public static bool Load(string fileName, Student student) {
            try {
                using (Stream stream = File.Open(fileName, FileMode.Open)) {
                    var binaryFormatter = new BinaryFormatter();
                    var s = (Student)binaryFormatter.Deserialize(stream);
                    student.FirstName = s.FirstName;
                    student.LastName = s.LastName;
                    student.BirthdayDate = s.BirthdayDate;
                    student.EducationForm = s.EducationForm;
                    student.GroupNumber = s.GroupNumber;
                    student.Exams = s.Exams;
                    student.Tests = s.Tests;
                }
            } catch (UnauthorizedAccessException) {
                Console.WriteLine("Error: Unauthorized access. Serialization has not been completed");
                return false;
            } catch (FileNotFoundException) {
                Console.WriteLine("Error: File not found. Serialization has not been completed");
                return false;
            }
            return true;
        }

        #endregion Methods
    }
}