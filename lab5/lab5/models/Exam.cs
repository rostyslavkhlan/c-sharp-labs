using System;

namespace lab5.models {
    [Serializable]
    public class Exam : IDateAndCopy<Exam> {
        public string SubjectName { get; set; }
        public int Mark { get; set; }
        public DateTime ExamDate { get; set; }

        public Exam() {
            SubjectName = "Default subject";
            Mark = 1;
            ExamDate = new DateTime();
        }

        public Exam(string subjectName, int mark, DateTime examDate) {
            SubjectName = subjectName;
            Mark = mark;
            ExamDate = examDate;
        }

        public override string ToString() {
            return SubjectName + " " + Mark + " " + ExamDate;
        }

        public Exam DeepCopy() {
            return (Exam)MemberwiseClone();
        }

        public DateTime Date { get; set; }

        protected bool Equals(Exam other) {
            return string.Equals(SubjectName, other.SubjectName) && Mark == other.Mark && ExamDate.Equals(other.ExamDate) && Date.Equals(other.Date);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Exam)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (SubjectName != null ? SubjectName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Mark;
                hashCode = (hashCode * 397) ^ ExamDate.GetHashCode();
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Exam left, Exam right) {
            return ReferenceEquals(left, right);
        }

        public static bool operator !=(Exam left, Exam right) {
            return !ReferenceEquals(left, right);
        }
    }
}