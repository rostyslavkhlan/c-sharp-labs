﻿using System;
using lab5.models;

namespace lab5 {
    internal static class Program {
        public static void Main() {

            var student = new Student();
            student.AddExams(new Exam(), new Exam());
            student.Save(@"C:\" + student.FirstName + student.LastName + ".txt");

            var s2 = new Student(new Person("Rostyslav", "khlan", DateTime.Now), Education.Bachelor, 205);
            s2.Load(@"C:\" + student.FirstName + student.LastName + ".txt");
            s2.AddFromConsole();
            Console.WriteLine(s2);

            Console.ReadKey();
        }
    }
}