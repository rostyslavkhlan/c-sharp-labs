using System;

namespace lab3 {
    public interface IDateAndCopy {
        object DeepCopy();
        DateTime Date { get; set; }
    }
    
    
}