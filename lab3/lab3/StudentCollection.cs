using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using lab3.models;

namespace lab3 {
    class StudentCollection {
        private const int DefaultsSize = 10;
        
        public List<Student> ListStudent { get; set; }
        
        public StudentCollection() : this(new List<Student>()) {
        }

        public StudentCollection(List<Student> listStudent){
            ListStudent = listStudent;
        }

        public double MaxAverageMark {
            get { return ListStudent.Max(mark => mark.AverageMark()); }
        }

        public IEnumerable<Student> Masters {
            get { return ListStudent.Where(education => education.EducationForm == Education.Master); }
        }

        public List<Student> GetStudentsByAverageMark(double value) {
            return ListStudent.Where(mark => Math.Abs(mark.AverageMark() - value) < 0.001).ToList();
        }
        
        public void AddDefaults() {
            for(var i = 0; i < DefaultsSize; ++i)
                ListStudent.Add(new Student());
        }

        public void AddStudents(params Student[] students) {
            if(students != null)
                ListStudent.AddRange(students);
        }

        public void SortByLastName() {
            ListStudent.Sort();
        }
        
        public void SortByBirthDate() {
            ListStudent.Sort(new Person());
            ListStudent.Sort(new Student());
        }
        
        public void SortByAverageMark() {
            ListStudent.Sort(new StudentComparer());
        }
        
        public override string ToString() {
            var stringBuilder = new StringBuilder();
            foreach (var student in ListStudent)
                stringBuilder.AppendLine(student.ToString());
            return stringBuilder + "";
        }

        public string ToShortString() {
            var stringBuilder = new StringBuilder();
            foreach (var student in ListStudent) {
                stringBuilder.AppendLine(student.ToShortString());
                stringBuilder.AppendLine("Total tests: " + student.Tests.Count + "");
                stringBuilder.AppendLine("Total exams: " + student.Exams.Count + "");
            }
            return stringBuilder + "";
        }
    }
}