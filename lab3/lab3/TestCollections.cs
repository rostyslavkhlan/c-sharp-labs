using System;
using System.Collections.Generic;
using System.Linq;
using lab3.models;

namespace lab3 {
    class TestCollections {
        private const int DefaultSize = 100000;
        
        public List<Person> ListPerson { get; set; }
        public List<string> ListString { get; set; }
        public Dictionary<Person, Student> DictionaryPerson { get; set; }
        public Dictionary<string, Student> DictionaryString { get; set; }
        
        public TestCollections() : this(new List<Person>(DefaultSize), new List<string>(DefaultSize), 
            new Dictionary<Person, Student>(DefaultSize), new Dictionary<string, Student>(DefaultSize)) {
        }
        
        public TestCollections(int size) : this(new List<Person>(size), new List<string>(size), 
            new Dictionary<Person, Student>(size), new Dictionary<string, Student>(size)) {
        }

        public TestCollections(List<Person> listPerson, List<string> listString, Dictionary<Person, Student> dictionaryPerson, Dictionary<string, Student> dictionaryString) {
            ListPerson = listPerson;
            ListString = listString;
            DictionaryPerson = dictionaryPerson;
            DictionaryString = dictionaryString;
        }

        public static Student GetStudent(int index) {
            var testCollections = new TestCollections(DefaultSize);
            for (var i = 0; i < DefaultSize; i++) {
                var person = new Person("First name " + i, "Last name " + i, new DateTime(2018,1,1));
                var student = new Student(person, Education.Bachelor, 500);
                testCollections.ListPerson.Add(person);
                testCollections.ListString.Add(person.ToString());
                testCollections.DictionaryPerson.Add(person, student);
                testCollections.DictionaryString.Add(person.ToString(), student);
            }

            var searchedPerson = new Person("First name " + index, "Last name " + index, new DateTime(2018,1,1));
            var searchedStudent = new Student(searchedPerson, Education.Bachelor, 500);
            
            Console.WriteLine("Searched person:");
            Console.WriteLine(searchedPerson);
            Console.WriteLine("Searched student:");
            Console.WriteLine(searchedStudent);

            var start = Environment.TickCount;
            testCollections.ListPerson.Find(x => x.Equals(searchedPerson));
            var end = Environment.TickCount;
            Console.WriteLine("List person: " + (end - start));
            
            start = Environment.TickCount;
            testCollections.ListString.Find(x => x.Equals(searchedPerson.ToString()));
            end = Environment.TickCount;
            Console.WriteLine("List string: " + (end - start));

            start = Environment.TickCount;
            testCollections.DictionaryPerson.Keys.Where(k => k.Equals(searchedPerson));
            end = Environment.TickCount;
            Console.WriteLine("Dictionary person(key): " + (end - start));
            
            start = Environment.TickCount;
            Student p;
            if(testCollections.DictionaryPerson.ContainsKey(searchedPerson))
                p = testCollections.DictionaryPerson[searchedPerson];
            end = Environment.TickCount;
            Console.WriteLine("Dictionary person(value): " + (end - start));
            
            start = Environment.TickCount;
            testCollections.DictionaryString.Keys.Where(k => k.Equals(searchedPerson.ToString()));
            end = Environment.TickCount;
            Console.WriteLine("Dictionary string(key): " + (end - start));
            
            start = Environment.TickCount;
            Student s = new Student();
            if(testCollections.DictionaryString.ContainsKey(searchedPerson.ToString()))
                s = testCollections.DictionaryString[searchedPerson.ToString()];
            end = Environment.TickCount;
            Console.WriteLine("Dictionary string(value): " + (end - start));

            return s;
        }
    }
}