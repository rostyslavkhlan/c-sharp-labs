﻿using System;
using lab3.models;

namespace lab3 {
    internal class Program {
        public static void Main(string[] args) {
            Student studentOne = new Student();
            Student studentTwo = new Student();
            Console.WriteLine(studentOne == studentTwo);
            Console.WriteLine(studentOne.Equals(studentTwo));
            
            
            /*var studentCollection = new StudentCollection();
            studentCollection.AddStudents(new Student(new Person("Vova", "WWW", new DateTime(2018,1,6)), Education.Bachelor, 107),
                                            new Student(new Person("Sash", "Pefds", new DateTime(2018,3,20)), Education.Bachelor, 507),
                                            new Student(new Person("QQQ", "Eeqwd", new DateTime(2014,5,20)), Education.Bachelor, 407),
                                            new Student(new Person("VVVVV", "Xcdasdw", new DateTime(2013,1,6)), Education.Bachelor, 307));
            Console.WriteLine(studentCollection);
            
            studentCollection.SortByLastName();
            Console.WriteLine(studentCollection);
            studentCollection.SortByBirthDate();
            Console.WriteLine(studentCollection);
            studentCollection.SortByAverageMark();
            Console.WriteLine(studentCollection);

            Console.WriteLine(studentCollection.MaxAverageMark);
            foreach(var student in studentCollection.Masters)
                Console.WriteLine(student);
            foreach(var student in studentCollection.GetStudentsByAverageMark(4))
                Console.WriteLine(student);
            
            Console.WriteLine("First element:");
            TestCollections.GetStudent(1);
            
            Console.WriteLine("Middle element:");
            TestCollections.GetStudent(5000);
            
            Console.WriteLine("Last element:");
            TestCollections.GetStudent(9999);
            
            Console.WriteLine("Not existed element:");
            Console.WriteLine(TestCollections.GetStudent(130000));*/
            
        }
    }
}