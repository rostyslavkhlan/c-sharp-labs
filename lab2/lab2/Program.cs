﻿using System;
using lab2.models;

namespace lab2 {
    internal static class Program {
        public static void Main() {

            var person1 = new Person();
            var person2 = new Person(); 

            Console.WriteLine(person1 == person2);
            Console.WriteLine(person1.Equals(person2));
            Console.WriteLine("First person hash code: " + person1.GetHashCode());
            Console.WriteLine("Second person hash code: " + person2.GetHashCode());
            
            var student = new Student();
            student.AddExams(new Exam("A", 5, DateTime.Now), new Exam("B", 2, DateTime.Now), new Exam("C", 5, DateTime.Now), new Exam("Math", 5, DateTime.Now));
            student.AddTests(new Test("A", true), new Test("Math", true), new Test("C", true));

            Console.WriteLine(student);
            Console.WriteLine(student.FirstName);

            var studentCopy = (Student) student.DeepCopy();
            ((Exam) studentCopy.Exams[0]).SubjectName = "Math";
            Console.WriteLine(student);
            Console.WriteLine(studentCopy);

            try {
                student.GroupNumber = 900;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            
            foreach (var s in student.GetExamsAbove(3)) {
                Console.WriteLine(s.ToString());
            }

            //Additionally
            foreach (string name in student)
                Console.WriteLine(name);
            
            foreach (var subject in student.GetPassedExamsAndTests())
                Console.WriteLine(subject);
            
            foreach (var test in student.GetPassedTestsByPassedExams())
                Console.WriteLine(test);
        }
    }
}