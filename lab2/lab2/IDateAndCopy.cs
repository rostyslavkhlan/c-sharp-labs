using System;

namespace lab2 {
    public interface IDateAndCopy {
        object DeepCopy();
        DateTime Date { get; set; }
    }
    
    
}