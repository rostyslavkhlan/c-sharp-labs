using System;

namespace lab2.models {
    public class Test : IDateAndCopy {
        public string SubjectName { get; set; }
        public bool Passed { get; set; }

        public Test() : this("Default name", false){}

        public Test(string subjectName, bool passed) {
            SubjectName = subjectName;
            Passed = passed;
        }

        public object DeepCopy() {
            return MemberwiseClone();
        }

        public DateTime Date { get; set; }
        
        
        public override string ToString() {
            return string.Format("SubjectName: {0}, Passed: {1}", SubjectName, Passed);
        }
    }
}