using System;

namespace lab2.models {
    public class Person : IDateAndCopy {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthdayDate { get; set; }

        public Person() : this("First name", "Last name", new DateTime()){}
        
        protected Person(Person person) {
            FirstName = person.FirstName;
            LastName = person.LastName;
            BirthdayDate = person.BirthdayDate;
        }
        
        public Person(string firstName, string lastName, DateTime birthdayDate) {
            FirstName = firstName;
            LastName = lastName;
            BirthdayDate = birthdayDate;
        }
        
        public int BirthdayDateYear {
            get { return BirthdayDate.Year; }
            set { BirthdayDate = new DateTime(value, BirthdayDate.Month, BirthdayDate.Day); }
        }
 
        public override string ToString() {
            return FirstName + " " + LastName + ". " + BirthdayDate;
        }
        
        public virtual string ToShortString() {
            return FirstName + " " + LastName;
        }
        
        protected bool Equals(Person other) {
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && BirthdayDate.Equals(other.BirthdayDate);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ BirthdayDate.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Person left, Person right) {
            return ReferenceEquals(left, right);
        }

        public static bool operator !=(Person left, Person right) {
            return !ReferenceEquals(left, right);
        }

        public virtual object DeepCopy() {
            return MemberwiseClone();
        }

        public virtual DateTime Date { get; set; }
    }
}