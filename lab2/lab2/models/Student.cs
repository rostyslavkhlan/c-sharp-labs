using System;
using System.Collections;
using System.Text;

namespace lab2.models {
    public class Student : Person, IEnumerable {
        public Education EducationForm { get; set; }
        private int _groupNumber;
        public ArrayList Exams { get; set; }
        public ArrayList Tests { get; set; }

        public Student() {
            EducationForm = Education.Bachelor;
            GroupNumber = 100;
            Exams = new ArrayList();
            Tests = new ArrayList();
        }

        public Student(Person person, Education educationForm, int groupNumber) : base(person) {
            EducationForm = educationForm;
            GroupNumber = groupNumber;
            Exams = new ArrayList();
            Tests = new ArrayList();
        }

        public IEnumerator GetEnumerator() {
            return new StudentEnumerator(Tests, Exams);
        }

        public IEnumerable GetExamsAbove(int required) {
            foreach (Exam exam in Exams) {
                if (exam.Mark > required) {
                    yield return exam;  
                }
            }
        }
        
        public IEnumerable GetPassedExamsAndTests() {
            foreach (var exam in GetExamsAbove(2)) {
                yield return exam;
            }
            foreach (Test test in Tests) {
                if (test.Passed) {
                    yield return test;  
                }
            }
        }
        
        public IEnumerable GetPassedTestsByPassedExams() {
            foreach (string name in this) {
                foreach (Test test in Tests) {
                    if (test.SubjectName.Equals(name) && test.Passed) {
                        foreach (Exam exam in Exams) {
                            if (exam.SubjectName.Equals(name) && exam.Mark > 2)
                                yield return test;
                        }
                    }
                }
            }
        }
        
        public int GroupNumber {
            get { return _groupNumber; }
            set {
                if(value < 100 || value > 699)
                    throw new Exception("Group number must be more than 100 and less than 699 ");
                _groupNumber = value;
            }
        }

        public bool this[Education education] {
            get { return education == EducationForm; }
        }
        
        public double AverageMark {
            get {
                if (Exams == null || Exams.Count == 0) return 0;
                var sum = 0;
                foreach (Exam exam in Exams) {
                    sum += exam.Mark;
                }
                return sum / Exams.Count;
            }
        }
        
        public void AddExams(params Exam[] exams) {
            Exams.AddRange(exams);
        }
        
        public void AddTests(params Test[] tests) {
            Tests.AddRange(tests);
        }
        
        public override string ToString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Exams:");
            foreach (Exam exam in Exams)
                stringBuilder.AppendLine(exam.ToString());
            stringBuilder.AppendLine("Tests:");
            foreach (Test test in Tests)
                stringBuilder.AppendLine(test.ToString());
            return stringBuilder.ToString();
        }

        public override string ToShortString() {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(base.ToShortString());
            stringBuilder.AppendLine(EducationForm + "");
            stringBuilder.AppendLine("Group number: " + GroupNumber);
            stringBuilder.AppendLine("Average mark: " + AverageMark);
            return stringBuilder.ToString();
        }

        public override object DeepCopy() {
            var other = (Student)MemberwiseClone();
            other.Exams = new ArrayList();
            foreach (Exam exam in Exams)
                other.Exams.Add(exam.DeepCopy());
            other.Tests = new ArrayList();
            foreach (Test test in Tests)
                other.Tests.Add(test.DeepCopy());
            return other;
        }

        protected bool Equals(Student other) {
            return base.Equals(other) && _groupNumber == other._groupNumber && EducationForm == other.EducationForm && Equals(Exams, other.Exams) && Equals(Tests, other.Tests);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Student) obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ _groupNumber;
                hashCode = (hashCode * 397) ^ (int) EducationForm;
                hashCode = (hashCode * 397) ^ (Exams != null ? Exams.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tests != null ? Tests.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Student left, Student right) {
            return ReferenceEquals(left, right);
        }

        public static bool operator !=(Student left, Student right) {
            return !ReferenceEquals(left, right);
        }
    }
}