using System;

namespace lab2.models {
    public class Exam : IDateAndCopy {
        public string SubjectName { get; set; }
        public int Mark { get; set; }
        public DateTime ExamDate { get; set; }
        
        public Exam() : this("Default subject", 1, new DateTime()){}

        public Exam(string subjectName, int mark, DateTime examDate) {
            SubjectName = subjectName;
            Mark = mark;
            ExamDate = examDate;
        }

        public override string ToString() {
            return SubjectName +  " " + Mark + " " + ExamDate;
        }

        public object DeepCopy() {
            return MemberwiseClone();
        }

        public DateTime Date { get; set; }
    }
}